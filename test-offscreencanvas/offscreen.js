function main() {  /* eslint consistent-return: 0 */
  const canvas = document.querySelector('#c');
  if (!canvas.transferControlToOffscreen) {
    canvas.style.display = 'none';
    document.querySelector('#noOffscreenCanvas').style.display = '';
    return;
  }
  const offscreen = canvas.transferControlToOffscreen();
  const worker = new Worker('https://threejsfundamentals.org/threejs/offscreencanvas-cubes.js', {type: 'module'});
  worker.postMessage({type: 'main', canvas: offscreen}, [offscreen]);

  function sendSize() {
    worker.postMessage({
      type: 'size',
      width: canvas.clientWidth,
      height: canvas.clientHeight,
    });
  }

  window.addEventListener('resize', sendSize);
  sendSize();
}
main();


